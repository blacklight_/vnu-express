#!/usr/bin/env bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

docker build -t npm-install $DIR/docker/npm_install

docker run -v $DIR:/app --rm npm-install