#!/usr/bin/env bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

docker run --rm -p 3000:3000 -v $DIR:/app keymetrics/pm2

