#VNU Server

##Install instructions
Docker must be installed, can be installed on ubuntu with the script at  docker/install_docker.bash

it creates a vnuadmin user that can access docker, but if you want to be able to use docker without sudo on you main 
user account then you should run `sudo usermod -aG docker USERNAME` after installing docker, where USERNAME is your username

After modifying your user you will need to log out then back in to your user account

`docker run hello-world` can be used to test if docker is installed correctly


###Database Setup

####DevCenter
I user Datastax DevCenter to interface with cassandra, you can find it [here](https://docs.datastax.com/en/developer/devcenter/doc/devcenter/dcInstallation.html)


## TODO
Convert legacy code to promises
