#!/usr/bin/env bash

#
# Creating a new database
#

mkdir ~/cassandradata

docker build -t vnu-cassandra .

export dbName=database-1
export dbBroadcastAddress=127.0.0.1
export dbDC=dev

docker run -d --name $dbName -e CASSANDRA_DC=$dbDC -e CASSANDRA_BROADCAST_ADDRESS=$dbBroadcastAddress -e CASSANDRA_SEEDS=$dbSeeds -p 7000:7000 -p 7001:7001 -p 7199:7199 -p 9042:9042 -p 9160:9160 -v cassandradata:/var/lib/cassandra/data vnu-cassandra

