#!/bin/bash
#####
##
## Install docker and create vnuadmin user
##
#####


sudo apt -y install curl linux-image-extra-$(uname -r) linux-image-extra-virtual

sudo apt -y install apt-transport-https ca-certificates

curl -fsSL https://yum.dockerproject.org/gpg | sudo apt-key add -

sudo add-apt-repository \
"deb https://apt.dockerproject.org/repo/ \
ubuntu-$(lsb_release -cs) \
main"

sudo apt update

sudo apt -y install git docker-engine=1.13.0-0~ubuntu-xenial

sudo adduser vnuadmin
sudo usermod -aG sudo vnuadmin
sudo usermod -aG docker vnuadmin