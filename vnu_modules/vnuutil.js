/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
 var Logger = require('../vnu_modules/logger.js');

var VnuUtil = {};

/*
    STATUS CODES for offline reference

 4×× Client Error
 400 Bad Request
 401 Unauthorized
 402 Payment Required
 403 Forbidden
 404 Not Found
 405 Method Not Allowed
 406 Not Acceptable
 407 Proxy Authentication Required
 408 Request Timeout
 409 Conflict
 410 Gone
 411 Length Required
 412 Precondition Failed
 413 Payload Too Large
 414 Request-URI Too Long
 415 Unsupported Media Type
 416 Requested Range Not Satisfiable
 417 Expectation Failed
 418 I'm a teapot
 421 Misdirected Request
 422 Unprocessable Entity
 423 Locked
 424 Failed Dependency
 426 Upgrade Required
 428 Precondition Required
 429 Too Many Requests
 431 Request Header Fields Too Large
 444 Connection Closed Without Response
 451 Unavailable For Legal Reasons
 499 Client Closed Request
 5×× Server Error
 500 Internal Server Error
 501 Not Implemented
 502 Bad Gateway
 503 Service Unavailable
 504 Gateway Timeout
 505 HTTP Version Not Supported
 506 Variant Also Negotiates
 507 Insufficient Storage
 508 Loop Detected
 510 Not Extended
 511 Network Authentication Required
 599 Network Connect Timeout Error
 */

VnuUtil.ErrorWithStatus = function ErrorWithStatus(reason, status, debugInfo) {
    var error = new Error(reason);
    error.status = status;
    error.debugInfo = debugInfo;        // Optional, later handle in logger
    return error;
};

/*
 *      Error from cassandra driver wrapper
 * takes err, an error from cassandra driver, logs real error and returns an
 * error to be shown to the client.
 *
 */
VnuUtil.CassandraError = function CassandraError(err, debugInfo) {
    err.debugInfo = debugInfo;        // Optional, later handle in logger
    Logger.cassandraError(err);
    var error = new Error("Database Error");
    return error;
}

module.exports = VnuUtil;
