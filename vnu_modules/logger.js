// from https://thejsf.wordpress.com/2015/01/18/node-js-logging-with-winston/

// var winston = require(‘winston’);
// winston.emitErrs = true;
// var logger = new winston.Logger({exitOnError: false});
//
// logger.addConsole = function(config) {
//     logger.add (winston.transports.Console, config);
// };
//
// logger.addFile = function(config) {
//     logger.add (winston.transports.File, config);
// };
//
// module.exports = logger;
//
// module.exports.stream = {
//     write: function(message, encoding){
//         logger.info(message);
//     }
// };

/*
 * Skeleton module that should be filled out later
 */

var logger = {};

logger.log = function(msg) {
    console.log(msg);
};

logger.debug = function(tag, msg) {
    console.log(tag, msg);
};

logger.info = function(msg) {
    console.log(msg);
}

logger.warn = function(msg) {
    console.log(msg);
}

logger.fatal = function(msg) {
    console.log(msg);
}

logger.error = function(msg) {
    console.log(msg);
}

logger.cassandraError = function(err) {
    console.log(err);
}

logger.handleError = function(err) {
    if (err) {
        console.log("Error: "+JSON.stringify(err));
        return;
    } else {
        return;
    }
};

module.exports = logger;