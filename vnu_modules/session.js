/**
 * Created by carter on 1/3/2017.
 */

/*
 * Sessions for the extension.
 * Tokens will be in the format: userid_randbase64str
 *  Userid as a uuid, then an underscore then a random base 64 token
 *
 * Written before CassandraDriver had promises, needs update
 *
 *
 */
var SESSION_DELIMITER = "_";

var BlUtil = require('./vnuutil.js');
var Crypto = require("crypto");
var CassandraDriver = require('cassandra-driver');

function Session(fullSessionId, newSession) {
    this.fullSessionId = fullSessionId;
    //console.log("Session id is ");
    //console.log(fullSessionId);
    var splitArr = (''+fullSessionId).split(SESSION_DELIMITER);

    // Verify exactly one occurence of SESSION_DELIMITER in the full session id
    if (splitArr.length != 2) throw new Error("Error splitting session string:'"+ fullSessionId +" by delimiter:'"+SESSION_DELIMITER+"', should have split into 2 strings but instead split into "+splitArr.length);

    // Add verification that splitArr[0] is a valid uuid is the future
    this.userid = splitArr[0];
    this.sessionid = splitArr[1];

    this.data = {};
    this.created = null;
    this.lastaccessed = null;
    this.state = Session.UNKNOWN;
    this.username = null;
    this.isadmin = null;

    if (newSession) {
        this.created = CassandraDriver.types.TimeUuid.now().getDate();
        this.state = Session.GOOD;
    }
}

// States
Session.UNKNOWN = -1;
Session.GOOD = 1;
Session.BAD = 2;
/*
 * Adds the user's information to the session
 * takes a user row directly from a database query
 */
Session.prototype.addUser = function (row) {
    this.phone = row.phone;
    this.isadmin = row.isadmin;
}

Session.prototype.getFromDatabase = function() {
    var self = this;

    return new Promise(function(fulfill, reject) {
        global.cassandra.execute('SELECT * FROM api_sessions WHERE userid = ? AND sessionid = ?', [self.userid, self.sessionid], function (err, result) {
            if (err) return reject(new BlUtil.CassandraError(err));
            else if (result.rowLength < 1) {
                return reject(new BlUtil.ErrorWithStatus("Session could not be found", 401));
            }
            var data = JSON.parse(result.rows[0].data);
            self.username = data.username;
            self.isadmin = data.isadmin;
            self.state = data.state;
            self.created = result.rows[0].created;
            self.lastaccessed = result.rows[0].lastaccessed;
            fulfill(self);
        });
    })
}

Session.prototype.save = function() {
    var self = this;

    return new Promise(function(fulfill, reject) {
        var now = CassandraDriver.types.TimeUuid.now().getDate();
        var sessionRow = {
            userid: self.userid,
            sessionid: self.sessionid,
            data: JSON.stringify({
                username: self.username,
                isadmin: self.isadmin,
                state: self.state
            }),
            created: self.created,
            lastaccessed: now
        }
        global.cassandra.execute('INSERT INTO api_sessions (userid, sessionid, data, created, lastaccessed) VALUES (:userid, :sessionid, :data, :created, :lastaccessed)', sessionRow, function(err) {
            if (err) return reject(new BlUtil.CassandraError(err));
            else fulfill(self);
        });
    });
}

Session.prototype.destroy = function() {
    var self = this;
    return new Promise(function(fulfill, reject) {
        global.cassandra.execute('DELETE FROM api_sessions WHERE userid = ? AND sessionid = ?', [self.userid, self.sessionid], function(err) {
            if (err) return reject(new BlUtil.CassandraError(err));
            else fulfill(self);
        });
    });
}

// Session.generateToken = function(userid) {
//     //var buffer = new Buffer(Crypto.randomBytes(64).toString());
//     Crypto.randomBytes(64, function(err, buffer) {
//         var token = buffer.toString('base64');
//     });
//
//     return userid + "_" + buffer.toString('base64');
// }

Session.newSession = function(userid) {
    return new Promise(function(fulfill, reject) {
        Crypto.randomBytes(64, function(err, buffer) {
            if (err) reject(err);

            var token = buffer.toString('base64');
            fulfill(new Session(userid+SESSION_DELIMITER+token, true));
        });
    });
}

module.exports = Session;

/*
 * Tests
 */

// function hasDuplicates(array) {
//     return (new Set(array)).size !== array.length;
// }
// var i = 0;
// var tokens = [];
// while (i < 500000) {
//     i++;
//     Session.newSession("Test-user-id").then(function (id){
//         tokens.push(id);
//         if (tokens.length == 500000) {
//             if (hasDuplicates(tokens))
//                 console.log("DUPLICATES FOUND");
//             else console.log("NO DUPLICATES FOUND");
//         }
//     });
// }
