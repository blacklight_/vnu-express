var config = require('../config');
var CassandraDriver = require('cassandra-driver');

config.cassandra.authProvider = new CassandraDriver.auth.PlainTextAuthProvider(config.cassandraAuth.user, config.cassandraAuth.password);
global.cassandra = new CassandraDriver.Client(config.cassandra);
var cassandra = global.cassandra;
//function User(id, email, passwordhash, isAdmin) {
function User(args) {
    this._class = this.constructor.name;

    // TODO: add validation for args

    this.id = args.id;
    this.email = args.email;
    this.passwordhash = args.passwordhash;
    this.isadmin = args.isadmin;
    this.state = args.state;
    this.datemodified = args.datemodified;

}

User.COLUMNFAMILY = "users";
User.CLASSNAME = "User";

User.prototype.constructor.name = User.CLASSNAME;

User.prototype.save = function() {
    var self = this;
    return new Promise(function(fulfill, reject) {
        // TODO: interpret the cassandra call output
        console.log("saving user ", self);
        cassandra.execute('INSERT INTO '+User.COLUMNFAMILY+' (id, passwordhash, email, isadmin, state, datemodified) VALUES ' +
            '(:id, :passwordhash, :email, :isadmin, :state, toTimestamp(now()))', self, { prepare: true }).then(function() {
                fulfill(self);
        }).catch(reject);
    });
}

module.exports = User;