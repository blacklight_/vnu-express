/**
 * Factory module for creating new data objects in the database
 *
 * Separated out into it's own class for the purpose of notifications
 */

var cassandra = global.cassandra;
var CassandraDriver = require('cassandra-driver');
var bcrypt = require('bcryptjs');
var config = require('../config');
var User = require('../vnu_modules/user.js');
var Show = require('../vnu_modules/show.js');
var Act = require('../vnu_modules/act.js');

// Region ids will be used for each city we are in. Hardcoding one for Ottawa to start
var ottawaRegionid = CassandraDriver.types.Uuid.fromString("023d5eb1-9508-43f4-8bdb-669c66a009e3");


Factory = {};

/*
 * args.owneruserid
 *
 */
Factory.newShow = function(args) {
    return new Promise(function(fulfill, reject) {
        var id = CassandraDriver.types.TimeUuid.now();

        data = {
            id: id,
            regionid: Show.REGIONS.OTTAWA,
            owneruserid: args.owneruserid,
            //ispublic: false,
            state: Show.STATES.PRIVATE,
            description: "",
            avatar: "",
            acts: [],
            genre: "",
            doors: "",
            age: "",
            location: "",
            price: "",
            date: Show.DEFAULT_DATE

        };

        var show = new Show(data);


        show.save().then(fulfill).catch(reject);



    });
}
/*
 * args = {
            avatar: "avatar url as a string",
            genre: "genre as a string, to be an object later",
            name: "act name as a string",
            admins: "list of user ids as strings that have administrative access to the act"
        }
 */
Factory.newAct = function(args) {
    return new Promise(function(fulfill, reject) {
        var id = CassandraDriver.types.TimeUuid.now();

        data = {
            id: id,
            regionid: Show.REGIONS.OTTAWA,
            avatar: args.avatar,
            genre: args.genre,
            name: args.name,
            admins: args.admins
        };

        var act = new Act(data);

        act.save().then(fulfill).catch(reject);

    });
}
/*
 * {
 *      email: "email as string",
 *      password: "plaintext password to be hashed as string"
 *  }
 */
Factory.newUser = function(args) {
    return new Promise(function(fulfill, reject) {
        var id = CassandraDriver.types.TimeUuid.now();

        bcrypt.hash(args.password, config.bcrypt.saltRounds).then(function(passwordHash) {

            var user = new User({
                id: id,
                email: args.email,
                passwordhash: passwordHash,
                isadmin: false,
                state: 0
            });

            user.save().then(fulfill).catch(reject);

        }).catch(reject);



    });
}

Factory.generateDemoData = function() {
    return new Promise(function(fulfill, reject) {

        Factory.newUser({email: "Retrac1234@gmail.com", password: "password"}).then(function(userCarter) {
            console.log("Created account: ", userCarter)

            Factory.newAct({
                avatar: "avatar url as a string",
                genre: "genre as a string",
                name: "Carter's act",
                admins: [userCarter.id]
            }).then(function(carterAct) {
                console.log("Created act: ", carterAct)
                Factory.newShow({
                    owneruserid: userCarter.id
                }).then(function(carterShow) {
                    console.log("Created show: ", carterShow)
                    carterShow.addAct(carterAct.id).then(function () {
                        carterShow.populateData().then(function () {
                            console.log("Show with populated data: ", carterShow)

                            fulfill();


                        }).catch(reject);
                    }).catch(reject);


                }).catch(reject);
            }).catch(reject);

        }).catch(reject);


    });
}

module.exports = Factory;