/**
 *
 * @constructor
 */
var cassandra = global.cassandra;
var CassandraDriver = require('cassandra-driver');
var Act = require('../vnu_modules/act');

function Show(args) {
    // TODO: make parameters more strict instead of general strings
    this.id = args.id;                                      // UUID of the show
    this.regionid = args.regionid;                              // UUID for the region of the event, currently hardcoded to ottawa
    this.owneruserid = args.owneruserid;
    this.acts = args.acts;                                       // acts is an array of references to user objects, but can be an array of strings for now
    this.state = args.state;
    this.date = args.date;                                       // dd/mm/yyyy format

    //this.data = {};
    //this.data.ispublic = args.ispublic;                         // changed to state

    if (args.hasOwnProperty('data')) {
        this.description = args.data.description                      // text description

        this.avatar = args.data.avatar;                                     // we should use a random picture url for nowthis.title: null, , title of venue or bar
        this.genre = args.data.genre;
        this.doors = args.data.doors;                                      // the time of day shring for now, object later
        this.age = args.data.age;                                        // string now, stricter later
        this.location = args.data.location;                                   // string indicating general, but not specific location i.e: “bridlewood area”, object later
        this.price = args.data.price;                                      // string now, sticter later
    } else {
        this.description = args.description                      // text description

        this.avatar = args.avatar;                                     // we should use a random picture url for nowthis.title: null, , title of venue or bar
        this.genre = args.genre;
        this.doors = args.doors;                                      // the time of day string for now, object later
        this.age = args.age;                                        // string now, stricter later
        this.location = args.location;                                   // string indicating general, but not specific location i.e: “bridlewood area”, object later
        this.price = args.price;                                      // string now, sticter later

    }



    this.content = true;                                    // Lucas asked for this for some reason?

    // To be later populated
    this.actData = {};

    this._class = this.constructor.name;
}



// Region ids will be used for each city we are in. Hardcoding one for Ottawa to start
Show.REGIONS = {
    OTTAWA: CassandraDriver.types.Uuid.fromString("023d5eb1-9508-43f4-8bdb-669c66a009e3")
}

Show.STATES = {
    PRIVATE: 0,
    PUBLIC: 1
}

Show.DEFAULT_DATE = new Date(2000, 0);
Show.COLUMNFAMILY = "shows"
Show.CLASSNAME = "Show"

Show.prototype.constructor.name = "Show";
Show.prototype._class = Show.CLASSNAME;
/*
 * Methods
 */

/*
 * This data object is the fields that get stored in the database in the data column instead of having their own columns.
 * they all must be strings and this function must include the same fields as the parseDataObj method.
 *
 * Each paramater must be a string in the returned data object (they can be another type as long as they can be parsed back
 * from the string in parseDataObj)
 *
 * This is just a temporary solution, most of these will later be turned into their own objects. This is just so we don't
 * have to change the database schema until something is final.
 */
Show.prototype.getDataObj = function() {
    const self = this;

    const data = {
        description: self.description,
        avatar: self.avatar,
        genre: self.genre,
        doors: self.doors,
        age: self.age,
        location: self.location,
        price: self.price,
    }

    return data;
}
Show.prototype.parseDataObj = function(data) {
    const self = this;


    self.description = data.description;
    self.avatar = data.avatar;
    self.genre = data.genre;
    self.doors = data.doors;
    self.age = data.age;
    self.location = data.location;
    self.price = data.price;


    return self;
}

Show.prototype.save = function() {
    var self = this;
    return new Promise(function(fulfill, reject) {
        // make a copy of the object to pass into the cassandra query
        // the data parameter is a map of parameter strings in the cassandra db that are not finalized enough to move to it's own column yet
        const copy = Object.assign({
            data: self.getDataObj()
        }, self);


        console.log("saving show ", self);
        // TODO: interpret the cassandra call output
        cassandra.execute('INSERT INTO '+Show.COLUMNFAMILY+' (id, regionid, owneruserid, acts, state, date, data, datemodified) VALUES ' +
            '(:id, :regionid, :owneruserid, :acts, :state, :date, :data, toTimestamp(now()))', copy, { prepare: true }).then(function() {
                fulfill(self);
        }).catch(reject);
    });
}

Show.prototype.addAct = function(actid) {
    const self = this;
    //const idAsTimeUUID = CassandraDriver.types.TimeUuid.fromString(actid.toString());

    return new Promise(function(fulfill, reject) {
        //console.log("self.acts.indexOf(actid)", self.acts.indexOf(actid))
        if (self.acts.indexOf(actid) === -1) {
            self.acts.push(actid);
            self.save().then(fulfill).catch(reject);
        } else {
            reject(new Error("Trying to add act that already exists in this show"));
        }

    })

};



/*
 * populates with act objects and such
 */
Show.prototype.populateData = function() {
    const self = this;
    return new Promise(function(fulfill, reject) {

        const promises = [];

        self.acts.forEach(function(actid) {
            promises.push(new Promise(function(fulfill, reject) {
                Act.getByID(actid).then(function(act) {
                    self.actData[actid] = act;
                    fulfill(self);
                }).catch(reject);
            }));
        });

        Promise.all(promises).then(function() {
            fulfill(self);
        }).catch(reject);
    });
}
/*
 * Static functions
 */

Show.getByID = function (user, regionid, showid) {
    return new Promise();
}

//TODO: add regionid and limit
Show.getRecentShows = function () {
    return new Promise(function(fulfill, reject) {
        cassandra.execute('SELECT * FROM '+Show.COLUMNFAMILY+' WHERE regionid = ?', [Show.REGIONS.OTTAWA]).then(function(resultSet) {
            const shows = [];
            const promises = [];
            //console.log("resultset ", resultSet)

            let i;
            for (i = 0; i < resultSet.rows.length; i++) {
                let show = new Show(resultSet.rows[i]);
                console.log("created", show);
                shows.push(show)
                promises.push(show.populateData());
            }

            Promise.all(promises).then(function() {
                fulfill(shows);
            }).catch(reject);

        });
    });
}

module.exports = Show;