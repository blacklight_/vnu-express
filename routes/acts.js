const express = require('express');
const router = express.Router();

const Factory = require('../vnu_modules/factory.js');
const Show = require('../vnu_modules/show');
const Act = require('../vnu_modules/act');

router.get('/', function (req, res, next) {
    res.json({});
});

router.get('/:actid', function(req, res, next) {
    var actid = req.params.actid;

    Act.getByID(actid).then(function(act) {
        console.log(act);
        res.json(act);
    }).catch(next);
});

router.post(':actid/update', function(req, res, next) {

})



module.exports = router;