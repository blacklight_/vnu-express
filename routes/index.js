const express = require('express');
const router = express.Router();

const Factory = require('../vnu_modules/factory.js');
const Show = require('../vnu_modules/show');
const Act = require('../vnu_modules/act');

router.get('/', function(req, res, next) {
  res.json("Index page");
});


/*
 * Populate the database with test data, only needs to be run upon initializing a new database
 */
router.get('/test', function(req, res, next) {
    Factory.generateDemoData().then(result =>

        res.render('index', { title: 'Express' })
    ).catch(next);

});

// Moved to act route

// router.get('/:actid', function(req, res, next) {
//     var actid = req.params.actid;
//
//     Act.getByID(actid).then(function(act) {
//         res.json(act);
//     }).catch(next);
// });

module.exports = router;
