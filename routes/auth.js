var express = require('express');
var router = express.Router();

//Node modules
var bcrypt = require('bcryptjs');
var JWT    = require('jsonwebtoken');
var UuidValidate = require('uuid-validate');
var CassandraDriver = require('cassandra-driver');

// Custom modules for vnu (vnu_modules/)
var Session = require('../vnu_modules/session.js');
var AnonymousSession = require('../vnu_modules/session.js');
var Logger = require('../vnu_modules/logger.js');
var VnuUtil = require('../vnu_modules/vnuutil.js');

// Config
var Config = require('../config');

/*
 * Custom send function
 *
 * Saves session then sends json
 */

router.use(function(req, res, next) {
    res.saveAndSend = function (json) {
        Logger.log("Save and send called");
        //Logger.log(json);
        req.session.save().then(function() {
            Logger.log("Save and send session saved");
            if (typeof json !== 'object') json = {};
            // Remove success boolean once the extention is updated to not need it

            res.json(json);
        }).catch(next);
    }
    next();
})


/*
 * Login query (/auth/login)
 *  Method: post
 *  body arguments:
 *      username
 *      password
 *
 *  response:
 *
 *      message
 *      token
 *
 */
router.post('/login', function(req, res, next) {

    if (req.body.phone == null || req.body.password == null) {
        return next(new VnuUtil.ErrorWithStatus("Invalid arguments for login", 400));
    } else {
        Logger.log("Attempting login for " + req.body.phone);
        cassandra.execute("SELECT *  FROM users WHERE phone=? LIMIT 1", [req.body.phone], function (err, result) {
            if (err) return next(new VnuUtil.CassandraError(err));
            // Check if user found
            else if (result.rowLength == 0) {
                Logger.log("User not found");
                return next(new VnuUtil.ErrorWithStatus("User not found", 404));
            }

            bcrypt.compare(req.body.password, result.rows[0].passwordhash).then(function(valid) {
                if (valid) {
                    Logger.log("Successfuly logged in as " + req.body.phone);


                    Session.newSession(result.rows[0].id).then(function (session) {
                        req.session = session;

                        req.session.addUser(result.rows[0]);

                        // Encode the session id as a json web token
                        var token = JWT.sign({sessionId: req.session.fullSessionId}, Config.secret, {
                            expiresIn: "90 days"
                        });
                        res.saveAndSend({
                            message: 'Successfully logged in!',
                            token: token
                        });
                    }).catch(next);
                } else {
                    Logger.log("Incorrect password for " + req.body.phone);

                    return next(new VnuUtil.ErrorWithStatus("Incorrect phone or password", 400));
                }
            }).catch(next);
        });
    }
});

/**
 * /anonymouslogin
 *
 * Arguments:
 *      Post: clientid - a UUID of the client
 *
 * Returns:
 *      format: JSON
 *          {
 *              anonymoustoken: a json web token containing the anonymous session id
 *          }
 */

router.post('/anonymouslogin', function(req, res, next) {

    var clientid = req.body.clientid;

    if (clientid =! null && UuidValidate(clientid)) {


        AnonymousSession.newSession(clientid).then(function (session) {
            req.session = session;

            req.session.addUser(result.rows[0]);

            // Encode the session id as a json web token
            var token = JWT.sign({sessionId: req.session.fullSessionId}, Config.secret, {
                expiresIn: "90 days"
            });
            res.saveAndSend({
                anonymoustoken: token
            });
        }).catch(next);
    } else {
        return next(new VnuUtil.ErrorWithStatus("Invalid arguments for login", 400));
    }
});

module.exports = router;